import demo.DemoService;
import entity.Library;
import service.LibraryService;

public class Main {

    public static void main(String[] args) {
        new DemoService(new LibraryService(), new Library()).execute();
    }
}
