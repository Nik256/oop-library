package demo;

import entity.Book;
import entity.Library;
import service.ILibraryService;

public class DemoService implements IDemoService{
    private ILibraryService libraryService;
    private Library library;

    public DemoService(ILibraryService libraryService, Library library) {
        this.libraryService = libraryService;
        this.library = library;
    }

    public void execute() {
        checkAdd();
        checkDelete();
        checkSearch();
    }

    @Override
    public void checkAdd() {
        System.out.println("Adding books");
        libraryService.addBook(library, new Book("Great Expectations", "Charles Dickens"));
        libraryService.addBook(library, new Book("The Road", "Cormac McCarthy"));
        libraryService.addBook(library, new Book("Outlander", "Diana Gabaldon"));
        libraryService.addBook(library, new Book("The Iliad", "Homer"));
        libraryService.addBook(library, new Book("Neverwhere", "Neil Gaiman"));
        System.out.println(library);
        if (!libraryService.addBook(library, new Book("The Picture of Dorian Gray", "Oscar Wilde")))
            System.out.println("Library is full!");
        System.out.println(library);
        System.out.println("===============================================");
    }

    @Override
    public void checkDelete() {
        System.out.println("Deleting books");
        Book book = libraryService.findBookByTitleAndAuthor(library, "The Iliad", "Homer");
        libraryService.deleteBook(library, book);
        System.out.println(library);
        libraryService.addBook(library, new Book("The Picture of Dorian Gray", "Oscar Wilde"));
        System.out.println(library);
        System.out.println("===============================================");
    }

    @Override
    public void checkSearch() {
        System.out.println("Finding books");
        Book book = libraryService.findBookByTitleAndAuthor(library, "The Road", "Cormac McCarthy");
        System.out.println(book);
        book = libraryService.findBookByTitleAndAuthor(library, "The Golden Compass", "Philip Pullman");
        System.out.println(book);
        System.out.println("===============================================");
    }
}
