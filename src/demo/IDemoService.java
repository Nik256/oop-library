package demo;

public interface IDemoService {
    void checkAdd();
    void checkDelete();
    void checkSearch();
}
