package service;

import entity.Book;
import entity.Library;

public interface ILibraryService {
    boolean addBook(Library library, Book book);
    boolean deleteBook(Library library, Book book);
    Book findBookByTitleAndAuthor(Library library, String title, String author);
}
